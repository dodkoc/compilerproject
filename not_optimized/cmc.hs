module Main where

import System.Environment(getArgs)
import System.IO

import Scan
import Types
import PrintTree
import Parse
import Identify
import TypeCheck
import CodeGeneration

main = do  
			args <- getArgs
			let inFile = head args
			inputHandle <- getAndOpenFile (inFile ++ ".cm") ReadMode
			scanOutputHandle <- getAndOpenFile (inFile ++ ".lex") WriteMode
			parseOutputHandle <- getAndOpenFile (inFile ++ ".parse") WriteMode
			identifyOutputHandle <- getAndOpenFile (inFile ++ ".id") WriteMode
			codeOutputHandle <- getAndOpenFile (inFile ++ ".code") WriteMode
			
			contents <- hGetContents inputHandle
			let tokens = scan [] contents
			writeTokensToFile scanOutputHandle tokens
			hClose scanOutputHandle
			putStr "Scanning done.\n"
			
			let syntaxTree = parseProgram tokens
			printDecls parseOutputHandle syntaxTree ""			
			hClose parseOutputHandle
			putStr "Parsing done.\n"
			
			let decoratedSyntaxTree = identify syntaxTree
			printDecls identifyOutputHandle decoratedSyntaxTree ""			
			hClose identifyOutputHandle
			putStr "Identification done.\n"	
			
			let typeCheckOk = typeCheck decoratedSyntaxTree
			if (typeCheckOk)
				then
					do
						putStr "Type checking done.\n"
						let code = codeGen decoratedSyntaxTree
						writeCodeToFile codeOutputHandle code 0
						hClose codeOutputHandle
						putStr "Code generation done.\n"
				else
					putStr "Never reached\n"
						
	
	
writeTokensToFile :: (Show t) => Handle -> [t] -> IO ()
writeTokensToFile h [] = hPutStr h ""
writeTokensToFile h (t:ts) = do	
								let str = show t
								hPutStr h (str ++ "\n")
								writeTokensToFile h ts

getAndOpenFile :: String -> IOMode -> IO Handle
getAndOpenFile name mode =
    do catch (openFile name mode)
             (\_ -> do error ("Cannot open "++ name ++ "\n"))
			 
			 
writeCodeToFile :: Handle -> [Instruction] -> Int -> IO()			 
writeCodeToFile h [] _ = hPutStr h ""
writeCodeToFile h (instruction:instructions) counter = 
	do
		hPutStr h ( (show counter) ++ ":\t\t" ++ (show instruction) ++ "\n" )
		writeCodeToFile h instructions (counter+1)
	