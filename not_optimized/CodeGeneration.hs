module CodeGeneration where

import Types
import Constants

-- Code Generation Helpers	
-- Returned code is without any optimizations, only two registers are used and
-- temporary area is in the activation record

-- decls
-- Parameters are list of declarations, Function Table, number of instructions already
-- generated, output is the list of generated insturctions and updated funTable	
cGenDecls :: [Decl] -> FunTable -> Int -> ([Instruction], FunTable)
cGenDecls [] funTable _ = ([], funTable)
cGenDecls (d:ds) funTable size = 
	do
		let (code, funTable1) = cGenDecl d funTable size 
		let (code1, funTable2) = cGenDecls ds funTable1 ((length code) + size)
		(code ++ code1, funTable2)

-- decl
-- FreeOffset for the stmt code is passed on as a positive number
-- and it's location after function parameters and local variables
cGenDecl :: Decl -> FunTable -> Int -> ([Instruction], FunTable)  		
cGenDecl (FunDecl _ id params stmt funLabel) funTable size = 
	do
		let funTable1 = funTable ++ [(funLabel, size)] -- (function label, address)
		let stmtCode = cGenStmt stmt funTable1 (2+(findSizeOfParamsAndLocalVarsInDecls params)+ findSizeOfParamsAndLocalVarsInStmt stmt )
		let code = [(RM ST ac retFO fp ("Begin function definition " ++ show id ++ ", store return address"))]
		let retCode = [(RM LD pc retFO fp "return to caller")]
		(code ++ stmtCode ++ retCode, funTable1)

-- variable declaration generates no code
cGenDecl (VarDecl _ _ _ _ _) funTable _ = ([], funTable) 

-- stmts		
		
cGenStmts :: [Stmt] -> FunTable -> Int -> [Instruction]
cGenStmts [] _ _ = []
cGenStmts (s:ss) funTable offset = 
	do
		let stmtCode = cGenStmt s funTable offset
		let stmtsCode = cGenStmts ss funTable offset
		stmtCode ++ stmtsCode
	    
-- stmt		
-- Parameters are Statement, Function Table, Free Offset
-- Retruns the code for each statement
cGenStmt :: Stmt -> FunTable -> Int -> [Instruction]
cGenStmt (ExpStmt exp) funTable offset = cGenExp exp funTable offset

cGenStmt (CompStmt decls stmts) funTable offset = 
	do
		-- no local functions are allowed, so this should not generate any code
		--let (declsCode, _) = cGenDecls decls funTable lastAddr
		let stmtsCode = cGenStmts stmts funTable offset
		--declsCode ++ 
		stmtsCode
		-- where
		--	(_, lastAddr) = last funTable

cGenStmt (IfStmt exp thenStmt elseStmt) funTable offset = 
	do
		let expCode = cGenExp exp funTable offset
		let thenStmtCode = cGenStmt thenStmt funTable offset
		let elseStmtCode = cGenStmt elseStmt funTable offset
		expCode ++ 
			[(RM JEQ ac ((length thenStmtCode)+1) pc "jump to else part on 0")] ++
			thenStmtCode ++
			[(RM LDA pc (length elseStmtCode) pc "jump over else part")] ++
			elseStmtCode

cGenStmt (WhileStmt exp stmt) funTable offset =
	do
		let expCode = cGenExp exp funTable offset
		let stmtCode = cGenStmt stmt funTable offset
		expCode ++ 
			[(RM JEQ ac ((length stmtCode)+1) pc "finish loop")] ++
			stmtCode ++
			[(RM LDA pc (-((length stmtCode) + (length expCode) + 2)) pc "return to loop expression check")]

cGenStmt (ReturnStmt exp) funTable offset = 
	do
		let expCode = cGenExp exp funTable offset
		expCode ++
			[(RM LD pc retFO fp "explicit return to caller")]


cGenStmt (NullStmt) funTable offset = []

-- exp	
-- Parameters are Exp, Function Table, Free Offset
-- Retruns the code for each expression
cGenExp :: Exp -> FunTable -> Int -> [Instruction]
cGenExp (NumExp num) _ _ = [(RM LDC ac num 0 "load numeric constant")]

-- id can be either global or local only
cGenExp (IdExp tok decl@(VarDecl _ _ _ idLevel idOffset)) funTable offset 
	| idLevel == Local = [(RM LD 0 idOffset fp ("load local id value " ++ show tok))]
	| otherwise = [(RM LD 0 idOffset gp ("load global id value " ++ show tok))]

cGenExp (AssignExp lhs rhs) funTable offset = 
	do
		let lhsCode = cGenIdAddress lhs funTable offset
		let tempStoreCode = [(RM ST ac (-offset) fp "store address in temporary area")]
		let rhsCode = cGenExp rhs funTable (offset+1)
		let assignCode = [(RM LD ac1 (-offset) fp "load id address from temporary area to ac1"),
						  (RM ST ac 0 ac1 "store value of assignment to address")]
		lhsCode ++ tempStoreCode ++ rhsCode ++ assignCode

-- specific code for each operator
-- arithmetic operations work only with registers
-- boolean operators are implemented via subtraction and then conditional jump
cGenExp (OpExp lhs op rhs) funTable offset 
	| op == PlusOp = do
		let plusCode = [(RO ADD ac ac1 ac "addition operation")]
		lhsCode ++ tempStoreValCode ++ rhsCode ++ opCode ++ plusCode
	| op == SubtractOp = do
		let subtractCode = [(RO SUB ac ac1 ac "subtraction operation")]
		lhsCode ++ tempStoreValCode ++ rhsCode ++ opCode ++ subtractCode
	| op == MultOp = do
		let multCode = [(RO MUL ac ac1 ac "multiplication operation")]
		lhsCode ++ tempStoreValCode ++ rhsCode ++ opCode ++ multCode
	| op == DivOp = do
		let divCode = [(RO DIV ac ac1 ac "division operation")]
		lhsCode ++ tempStoreValCode ++ rhsCode ++ opCode ++ divCode
	| op == NeqOp = do
		let testCode = [(RM JNE ac 2 pc "jump to set True on NeqOp")]
		lhsCode ++ tempStoreValCode ++ rhsCode ++ opCode ++ 
			boolBeginCode ++ testCode ++ boolEndCode
	| op == EqOp = do
		let testCode = [(RM JEQ ac 2 pc "jump to set True on EqOp")]
		lhsCode ++ tempStoreValCode ++ rhsCode ++ opCode ++ 
			boolBeginCode ++ testCode ++ boolEndCode
	| op == GeOp = do
		let testCode = [(RM JGE ac 2 pc "jump to set True on GeOp")]
		lhsCode ++ tempStoreValCode ++ rhsCode ++ opCode ++ 
			boolBeginCode ++ testCode ++ boolEndCode
	| op == LeOp = do
		let testCode = [(RM JLE ac 2 pc "jump to set True on LeOp")]
		lhsCode ++ tempStoreValCode ++ rhsCode ++ opCode ++ 
			boolBeginCode ++ testCode ++ boolEndCode
	| op == GtOp = do
		let testCode = [(RM JGT ac 2 pc "jump to set True on GtOp")]
		lhsCode ++ tempStoreValCode ++ rhsCode ++ opCode ++ 
			boolBeginCode ++ testCode ++ boolEndCode
	| op == LtOp = do
		let testCode = [(RM JLT ac 2 pc "jump to set True on LtOp")]
		lhsCode ++ tempStoreValCode ++ rhsCode ++ opCode ++ 
			boolBeginCode ++ testCode ++ boolEndCode
		where
			lhsCode = cGenExp lhs funTable offset
			tempStoreValCode = [(RM ST ac (-offset) fp "store value in temporary area")]
			rhsCode = cGenExp rhs funTable (offset+1)
			opCode = [(RM LD ac1 (-offset) fp "load stored value from temporary area to ac1")]
			boolBeginCode = [(RO SUB ac ac1 ac "subtraction operation")]
			boolEndCode = [(RM LDC ac 0 0 "set False"),
					  	(RM LDA pc 1 pc "jump over next instruction"),
					  	(RM LDC ac 1 0 "set True")]

cGenExp (ArrayExp tok decl subs) funTable offset = 
	do
		let addrCode = cGenIdAddress (ArrayExp tok decl subs) funTable offset
		let arrayCode = [(RM LD ac 0 ac "load value of array node")]
		addrCode ++ arrayCode

cGenExp (CallExp tok decl@(FunDecl _ _ _ _ label) args) funTable offset = 
	do
		let argsCode = cGenArgs args funTable (offset-initFO)
		let funAddress = liftJust(lookup label funTable)
		let callCode = [(RM LDC ac1 0 0 "load constant 0 to ac1"),
						(RM ST fp (-offset-ofpFO) fp "save current fp at ofpFO"),
						(RM LDA fp (-offset) fp "set new frame"),
						(RM LDA ac 1 pc "load ac with return address"),
						(RM LDA pc (funAddress) ac1 ("jump to function entry" ++ show tok)),
						(RM LD fp ofpFO fp "pop current frame")]
		argsCode ++ callCode

cGenExp (NullExp) _ _  = []

-- Function for generation call arguments
cGenArgs :: [Exp] -> FunTable -> Int -> [Instruction]
cGenArgs [] _ _ = []

-- Specific case of ID expression, for Array and Parray the address needs to be stored
-- For any other expression value is stored
cGenArgs ((IdExp tok decl@(VarDecl declType _ _ _ _)):es) funTable offset
	| (declType == TypeParray) || (declType == TypeArray) = do
		let eCode = cGenIdAddress (IdExp tok decl) funTable offset
		eCode ++ argValCode ++ esCode
	| otherwise = do
		let eCode = cGenExp (IdExp tok decl) funTable offset
		eCode ++ argValCode ++ esCode
		where
			argValCode = [(RM ST ac (-offset) fp "store arg value")]
			esCode = cGenArgs es funTable (offset+1)
	
cGenArgs (e:es) funTable offset =
	do
		let eCode = cGenExp e funTable offset
		let argValCode = [(RM ST ac (-offset) fp "store arg value")]
		let esCode = cGenArgs es funTable (offset+1)
		eCode ++ argValCode ++ esCode

-- Function generates the code for ID address used for array operation and call arguments
cGenIdAddress :: Exp -> FunTable -> Int -> [Instruction]

-- Array is treated as normal local/global variable for which base address is saved
-- In case of Parray, the address of array is stored in the address of local variable
cGenIdAddress (IdExp tok decl@(VarDecl idType _ _ idLevel idOffset)) funTable offset
	| idType == TypeParray = [(RM LD ac idOffset fp ("load address of passed parray " ++ show tok))]
	| idLevel == Local = [(RM LDA ac idOffset fp ("load local id address " ++ show tok))]
	| otherwise =  [(RM LDA ac idOffset gp ("load global id address " ++ show tok))]

-- Array expression first computes the value of exression in the subscript
-- bund check only checks if subscript is not less than 0
-- in Parray, adress of the actual array is stored in the local variable
cGenIdAddress (ArrayExp tok decl@(VarDecl idType _ idSize idLevel idOffset) exp) funTable offset 
	| (idType == TypeArray) && (idLevel == Local) = do
		let baseAddressCode = [(RM LDA ac1 idOffset fp ("load base of local array " ++ show tok))]
		expCode ++ boundCheckCode ++ baseAddressCode ++ reqElemCode
	| (idType == TypeArray) && (idLevel == Global) = do
		let baseAddressCode = [(RM LDA ac1 idOffset gp ("load base of global array " ++ show tok))]
		expCode ++ boundCheckCode ++ baseAddressCode ++ reqElemCode
	| otherwise = do
		let baseAddressCode = [(RM LD ac1 idOffset fp ("load base of parray " ++ show tok))]
		expCode ++ boundCheckCode ++ baseAddressCode ++ reqElemCode
		where
			expCode = cGenExp exp funTable offset
			boundCheckCode = [(RM JGE ac 1 pc "jump over halt on subscript >= 0"),
							  (RO HALT 0 0 0 "halt on negative array subscript")] 
			reqElemCode = [(RO SUB ac ac1 ac "apply subscript to base address")]


-- Main Code Generation Function	

codeGen :: SyntaxTree -> [Instruction]
codeGen syntaxTree = 
	do
		let funTable = [(0,sizeOfPreludeCode+5), (1, sizeOfPreludeCode+5+sizeOfInputFunCode)]
		let instructionCount = sizeOfPreludeCode + sizeOfInputFunCode + sizeOfOutputFunCode + 5
		let (instructions, retFunTable) = cGenDecls syntaxTree funTable instructionCount
		let (_,mainAddr) = last retFunTable
		let globalArea = findSizeOfGlobalArea syntaxTree
		let setFrame = [(RM LDA fp (-globalArea) fp "set new frame")]
		let callMain = [(RM LDC ac1 0 0 "load constant 0 to ac1"),
					(RM LDA ac 1 pc "load ac with return address from 'main'"),
					(RM LDA pc mainAddr ac1 "jump to function 'main'"),
					(RO HALT 0 0 0 "")]

		preludeCode ++ setFrame ++ callMain ++ inputFunCode ++ outputFunCode ++ instructions
	
-- Size helpers	
-- These functions return the size of area in memory that is reserved for variables
-- size of scalar is 1, size of an array is stored in the declaration
findSizeOfGlobalArea :: [Decl] -> Int
findSizeOfGlobalArea [] = 0
findSizeOfGlobalArea ((FunDecl _ _ _ _ _):ds) = findSizeOfGlobalArea ds
findSizeOfGlobalArea ((VarDecl _ _ size _ _ ):ds) 
	| size == 0 = 1 + (findSizeOfGlobalArea ds)
	| otherwise = size + (findSizeOfGlobalArea ds)

findSizeOfParamsAndLocalVarsInDecl :: Decl -> Int
findSizeOfParamsAndLocalVarsInDecl (FunDecl _ _ decls stmt _ ) =
	(findSizeOfParamsAndLocalVarsInDecls decls) + (findSizeOfParamsAndLocalVarsInStmt stmt)

findSizeOfParamsAndLocalVarsInDecl (VarDecl _ _ size _ _) = 
	if (size == 0) then 1
		else size

findSizeOfParamsAndLocalVarsInDecls :: [Decl] -> Int
findSizeOfParamsAndLocalVarsInDecls [] = 0
findSizeOfParamsAndLocalVarsInDecls (d:ds) =
	do 
		let dSize = findSizeOfParamsAndLocalVarsInDecl d
		let dsSize = findSizeOfParamsAndLocalVarsInDecls ds 
		dSize + dsSize

-- local vars declarations can only be present in CompStmt
findSizeOfParamsAndLocalVarsInStmt :: Stmt -> Int
findSizeOfParamsAndLocalVarsInStmt (CompStmt decls stmts) = 
	(findSizeOfParamsAndLocalVarsInDecls decls) + (findSizeOfParamsAndLocalVarsInStmts stmts)

findSizeOfParamsAndLocalVarsInStmt (IfStmt _ stmt1 stmt2) =
	(findSizeOfParamsAndLocalVarsInStmt stmt1) + (findSizeOfParamsAndLocalVarsInStmt stmt2)

findSizeOfParamsAndLocalVarsInStmt (WhileStmt _ stmt) = 
	findSizeOfParamsAndLocalVarsInStmt stmt 

findSizeOfParamsAndLocalVarsInStmt _ = 0

findSizeOfParamsAndLocalVarsInStmts :: [Stmt] -> Int
findSizeOfParamsAndLocalVarsInStmts [] = 0
findSizeOfParamsAndLocalVarsInStmts (s:ss) = 
	do
		let sSize = findSizeOfParamsAndLocalVarsInStmt s
		let ssSize = findSizeOfParamsAndLocalVarsInStmts ss 
		sSize + ssSize


-- Static code	
	
preludeCode = 
	[(RM LD gp 0 ac "load gp with maxaddress"),
	 (RM LDA fp 0 gp "copy gp to fp"),
	 (RM ST ac 0 ac "clear location 0")]
sizeOfPreludeCode = length preludeCode
	 
outputFunCode =
	[(RM ST ac retFO fp "Begin built-in function definition 'output', store return address"),
	 (RM LD ac initFO fp "load output value"),
	 (RO OUT ac 0 0 "output"),
	 (RM LD pc retFO fp "End built-in function definition 'output', return to caller")]
sizeOfOutputFunCode = length outputFunCode

inputFunCode = 
	[(RM ST ac retFO fp "Begin built-in function definition 'input', store return address"),
	 (RO IN ac 0 0 "input"),
	 (RM LD pc retFO fp "End built-in function definition 'input', return to caller")]
sizeOfInputFunCode = length inputFunCode







		
		
		
		
