module Identify where

import SymTab
import Types
import Debug.Trace

-- Function goes throught the Syntax tree and returns updated Syntax tree with missing declarations
identify :: SyntaxTree -> SyntaxTree
identify synTree =
	do
		let symTab = stInitial
		let (newSynTree, _) = identifyDecls synTree symTab
		newSynTree
	
-- decls	
	
identifyDecls :: [Decl] -> SymbolTable -> ([Decl], SymbolTable)
identifyDecls [] st = ([], st)
identifyDecls (d:ds) st =
	do
		let (d1, st1) = identifyDecl d st
		let (ds1, st2) = identifyDecls ds st1
		([d1] ++ ds1, st2)

-- stmts	
	
identifyStmts :: [Stmt] -> SymbolTable -> ([Stmt], SymbolTable)		
identifyStmts [] st = ([], st)
identifyStmts (s:ss) st =
	do
		let (s1, st1) = identifyStmt s st
		let (ss1, st2) = identifyStmts ss st1
		([s1] ++ ss1, st2)

-- exps		
		
identifyExps :: [Exp] -> SymbolTable -> ([Exp], SymbolTable)			
identifyExps [] st = ([], st)
identifyExps (e:es) st =
	do
		let (e1, st1) = identifyExp e st
		let (es1, st2) = identifyExps es st1
		([e1] ++ es1, st2)

-- decl		
-- Just inserts the variable into the symbol table wihout the change of scope
-- Redeclaration error is raised if insert failed
identifyDecl :: Decl -> SymbolTable -> (Decl, SymbolTable)			
identifyDecl (VarDecl arrayType id size level offset) ts =
	do
		let (d, ts1) = stInsert (VarDecl arrayType id size level offset) ts
		if ts1 == [] 
			then redeclError d id
			else (d, ts1)

-- Begins new scope with new offset in which the parameters are defined
-- function that handles compound statements creates new local scope 
identifyDecl (FunDecl returnType id declList compStmt address) ts =
	do
		let (d@(FunDecl _ _ _ _ addr), ts1) = stInsert (FunDecl returnType id declList compStmt address) ts
		if ts1 == [] 
			then redeclError d id
			else do
				let ts2 = stBeginScopeNewOffset ts1
				let (ds, ts3) = identifyDecls declList ts2
				--let ts4 = stBeginScope ts3 
				let (s, ts5) = identifyStmt compStmt ts3
				--let ts6 = stEndScope ts5
				let ts7 = stEndScopeNewOffset ts5
				((FunDecl returnType id ds s addr), ts7)
						
-- stmt			
		
identifyStmt :: Stmt -> SymbolTable -> (Stmt, SymbolTable)		
identifyStmt (ExpStmt exp) ts = ((ExpStmt e), ts1)
	where (e, ts1) = identifyExp exp ts

-- Creates new scope for every compound statement in {}
identifyStmt (CompStmt declList stmtList) ts =
	do
		let ts1 = stBeginScope ts
		let (d, ts2) = identifyDecls declList ts1
		let (s, ts3) = identifyStmts stmtList ts2
		let ts4 = stEndScope ts3
		((CompStmt d s), ts4)

identifyStmt (IfStmt exp thenStmt elseStmt) ts =
	do
		let (e, ts1) = identifyExp exp ts
		let (thenS, ts2) = identifyStmt thenStmt ts1
		let (elseS, ts3) = identifyStmt elseStmt ts2
		((IfStmt e thenS elseS), ts3)

identifyStmt (WhileStmt exp stmt) ts =
	do
		let (e, ts1) = identifyExp exp ts
		let (s, ts2) = identifyStmt stmt ts1
		((WhileStmt e s), ts2)

identifyStmt (ReturnStmt exp) ts =
	do
		let (e, ts1) = identifyExp exp ts 
		((ReturnStmt e), ts1)

identifyStmt (NullStmt) ts = (NullStmt, ts)


-- exp	
-- Symbol table does not change in these functions
identifyExp :: Exp -> SymbolTable -> (Exp, SymbolTable)	
identifyExp (AssignExp var exp) ts =
	do
		let (v, ts1) = identifyExp var ts
		let (e, ts2) = identifyExp exp ts1
		((AssignExp v e), ts2)

identifyExp (OpExp lExp operator rExp) ts = 
	do
		let (lE, ts1) = identifyExp lExp ts
		let (rE, ts2) = identifyExp rExp ts1
		((OpExp lE operator rE), ts2)

-- Searches for the name in the Symbol table
-- Missing declaration error is raised if no name was found
identifyExp (IdExp id decl) ts = 
	do 
		let (Id name, _, _) = id
		let found = stLookup name ts
		if found /= Nothing	
			then do
				let d = liftJust found
				((IdExp id d), ts)
			else missingDeclError id

identifyExp (ArrayExp id decl exp) ts = 
	do 
		let (Id name, _, _) = id
		let found = stLookup name ts
		if found /= Nothing	
			then do
				let d = liftJust found
				let (e, ts1) = identifyExp exp ts
				((ArrayExp id d e), ts)
			else missingDeclError id

identifyExp (CallExp id decl expList) ts =
	do 
		let (Id name, _, _) = id
		let found = stLookup name ts
		if found /= Nothing 
			then do
				let d = liftJust found
				let (e, ts1) = identifyExps expList ts
				((CallExp id d e), ts1)
			else missingDeclError id

identifyExp (NumExp num) ts = ((NumExp num), ts)
identifyExp (NullExp) ts = ((NullExp), ts)


------------------
-- error report --
------------------
-- Redeclaration error, parameters are declaration of function returned from stInsert and
-- token of the inserted declaration to provide useful information about position of 
-- redeclared functions and variables
redeclError ((VarDecl _ (Id declName, declL, _) _ _ _)) (_, idL, _) = error ("Redeclaration of variable \"" ++ declName ++ "\" from line " ++ show declL ++ " on line " ++ show idL ++"\n")
redeclError ((FunDecl _ (Id declName, declL, _) _ _ _)) (_, idL, _) = error ("Redeclaration of function \"" ++ declName ++ "\" from line " ++ show declL ++ " on line " ++ show idL ++"\n")

-- Missing declaration error, parameter is the ID of the expression to provide name and
-- position of the token in the error message
missingDeclError (Id name, l, c) = error ("Missing declaration of \"" ++ name ++ "\" in line " ++ show l ++"\n")