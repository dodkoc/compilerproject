module Scan where

import Types
import Constants
import Data.Char

----------------------------	
-- Main Scanning Function --
----------------------------	
	   
scan :: String -> String -> [Token]	   
scan lStr rStr

	-- End of input (base case) return EOF token.
	| rStr == []          
		= let (row, col) = getRowCol lStr in
			[(EOF, row, col)]
			
	------- WHITESPACE -------
	-- Note: checked first to prevent last being called on null lStr
	
	| firstIsWhite rStr   
		= scan (lStr ++ [head rStr]) (tail rStr)			
	 
	-------- COMMENTS --------
	-- Note: lexComment appears before lexSymbol so a comment is scanned instead of the opening symbols 
	-- Note: comments add no tokens to the list.	 

	| firstIsComment rStr 
		= let (lStrNew, rStrNew) = lexComment lStr rStr in
			scan lStrNew rStrNew

	-------- SYMBOL -------- 
	
	| firstIsSymbol rStr  
		= let (tok, lStrNew, rStrNew) = recogSymbol lStr rStr in
			(tok : (scan lStrNew rStrNew))	  		 
	 
	-------- IDENTIFIER & KEYWORD --------
	-- Note: Short-circuiting is used so last is not evaled on null lStr.
	-- Case 1 - ID or reserved word at beginning of program:
	--    leftString is empty and first char in rightString is letter.	   
	-- Case 2 - ID or reserved word in middle of program:
	--    Last char in leftString is not digit and first char in rightString is letter.
	--                                 This \/ short circuit test is used to prevent last being called on [] when first char in file is digit.
	
	| (lStr == [] && firstIsLetter rStr) || ((not (firstIsDigit rStr)) && (not (firstIsDigit [last lStr]) && firstIsLetter rStr))
		= let (tok, lStrNew, rStrNew) = recogReservedOrId lStr rStr in
			(tok : (scan lStrNew rStrNew))
	
	-------- NUMBER --------
	-- Note: Short-circuiting is used so last is not evaled on null lStr.
	-- Case 1 - Number at beginning of program:
	--    leftString is empty and first char in rightString is digit.	   
	-- Case 2 - Number in middle of program:
	--    Last char in leftString is not letter and first char in rightString is digit.			
		
	| (lStr == [] && firstIsDigit rStr) || (not (firstIsLetter [last lStr]) && firstIsDigit rStr)
		= let (tok, lStrNew, rStrNew) = recogNum lStr rStr in
			(tok : (scan lStrNew rStrNew))		
	 
	-- Lexical error in the input 
	| otherwise          
		= errorUnexpectedChar lStr (head rStr) []

	  
-----------------------	
-- Predicate Helpers --
-----------------------	  
-- added test for empty xs in firstIsComment
firstIsComment :: String -> Bool 
firstIsComment [] = False
firstIsComment (x:xs) = (x == '/') && (not (null xs)) && ((head xs) == '*')

firstIsSymbol :: String -> Bool
firstIsSymbol [] = False   
firstIsSymbol (x:xs) = elem x "+-*/<>=!;,()[]{}"

firstIsDigit :: String -> Bool
firstIsDigit [] = False
firstIsDigit (x:xs) = elem x "0123456789"	   

firstIsLetter :: String -> Bool
firstIsLetter [] = False	   
firstIsLetter (x:xs) = (x >= 'A' && x <= 'Z') || (x >= 'a' && x <= 'z')

firstIsWhite :: String -> Bool
firstIsWhite [] = False
firstIsWhite (x:xs) = elem x " \t\n"


-----------------	
-- Scan Symbol --
-----------------	 
-- Function takes lStr and rStr as input and returns a tuple containing 
-- recoginzed token and the new lStr and rStr
-- lStr is input that's already been recognized
-- rStr input that's not been recognized yet
recogSymbol :: String -> String -> (Token, String, String)
recogSymbol lStr rStr = 
	let (row, col) = getRowCol lStr in
		lexSymbol lStr rStr row col

-- Function takes lStr, rStr and additional row and collumn numbers,
-- these are used to store the precise position of the token in the string
lexSymbol :: String -> String -> Int -> Int -> (Token, String, String)	  
lexSymbol lStr (x:rStr) row col
	-- Multi char symbols
	-- checked first, so that the longest possible symbol is matched
	-- checks also for empty rStr to prevent error from calling head on empty string
	| x == '=' && (not (null rStr)) && (head rStr == '=')
		= ((Eq, row, col), (lStr ++ [x] ++ [head rStr]), tail rStr)
	| x == '<' && (not (null rStr)) && (head rStr == '=')	
		= ((Le, row, col), (lStr ++ [x] ++ [head rStr]), tail rStr)
	| x == '>' && (not (null rStr)) && (head rStr == '=')
		= ((Ge, row, col), (lStr ++ [x] ++ [head rStr]), tail rStr)
	| x == '!' && (not (null rStr)) && (head rStr == '=')	
		= ((Neq, row, col), (lStr ++ [x] ++ [head rStr]), tail rStr)
	
	-- Single char symbols
	-- simply matches the symbol with the correct token and moves the char from rStr to lStr
	| x == '+'
		= ((Plus, row, col), (lStr ++ [x]), rStr)
	| x == '-'
		= ((Subtract, row, col), (lStr ++ [x]), rStr)
	| x == '*'
		= ((Mult, row, col), (lStr ++ [x]), rStr)
	| x == '/'
		= ((Div, row, col), (lStr ++ [x]), rStr)
	| x == '<'
		= ((Lt, row, col), (lStr ++ [x]), rStr)
	| x == '>'
		= ((Gt, row, col), (lStr ++ [x]), rStr)
	| x == '='
		= ((Assign, row, col), (lStr ++ [x]), rStr)
	| x == ';'
		= ((Semi, row, col), (lStr ++ [x]), rStr)
	| x == ','
		= ((Comma, row, col), (lStr ++ [x]), rStr)
	| x == '('
		= ((Lparen, row, col), (lStr ++ [x]), rStr)
	| x == ')'
		= ((Rparen, row, col), (lStr ++ [x]), rStr)
	| x == '['
		= ((Lbracket, row, col), (lStr ++ [x]), rStr)
	| x == ']'
		= ((Rbracket, row, col), (lStr ++ [x]), rStr)
	| x == '{'
		= ((Lbrace, row, col), (lStr ++ [x]), rStr)
	| x == '}'
		= ((Rbrace, row, col), (lStr ++ [x]), rStr)

	-- Lexical error in the input  
	| otherwise
		= errorUnexpectedChar lStr x rStr  
	  
	  
------------------	
-- Scan Comment --
------------------	 	  
-- Consumes the entire comment
-- starting sequence /* and closing sequence */ are treated as one symbol

lexComment :: String -> String -> (String, String)
lexComment lStr [] = errorInputEndedEarly lStr
lexComment lStr (x:rStr)
	| null rStr
		= errorInputEndedEarly lStr
	
	| (x == '*') && ((head rStr) == '/')
		= (lStr ++ [x] ++ [head rStr], tail(rStr))
	
	| firstIsComment (rStr) 
		= errorUnexpectedChar (lStr++[x]) (head rStr) (tail rStr)

	-- additional test for /*/* must be performed right after removing comment starting symbol
	| (x == '/') && ((head rStr) == '*')
		= if firstIsComment (tail rStr) 
			then errorUnexpectedChar (lStr++ [x] ++ [head rStr]) (head (tail rStr)) (tail(tail rStr))
			else lexComment (lStr ++ [x] ++ [head rStr]) (tail rStr)
	
	| otherwise		
		= lexComment (lStr ++ [x]) rStr
	 
--------------------------------------
-- Scan Reserved Word or Identifier --
--------------------------------------
-- Function recognises reserved words and identifiers in that order 
recogReservedOrId :: String -> String -> (Token, String, String)
recogReservedOrId lStr rStr 
	| s == "else"
		= ((Else, row, col), lStr ++ take (length s) rStr, drop (length s) rStr)	
	| s == "if"
		= ((If, row, col), lStr ++ take (length s) rStr, drop (length s) rStr)
	| s == "int"
		= ((Int, row, col), lStr ++ take (length s) rStr, drop (length s) rStr)
	| s == "return"
		= ((Return, row, col), lStr ++ take (length s) rStr, drop (length s) rStr)
	| s == "while"
		= ((While, row, col), lStr ++ take (length s) rStr, drop (length s) rStr)
	| s == "void"
		= ((Void, row, col), lStr ++ take (length s) rStr, drop (length s) rStr)
	| otherwise	
		= ((Id (s), row, col), lStr ++ take (length s) rStr, drop (length s) rStr)
	where (row, col) = getRowCol lStr;
	       s = lexString lStr rStr

-- Helper function to recognize identifiers, takes lStr and rStr as input
-- returns the longest string of letters defined by function firstIsLetter
lexString :: String -> String -> String
lexString _ [] = []
lexString lStr (x:rStr)
	| not (firstIsLetter [x])
		= []
	| otherwise 
		= x : lexString	(lStr ++ [x]) rStr	
		
-----------------
-- Scan Number --
-----------------
-- Function takes lStr and rStr as input and returns token of number
-- number in the token is stored as an Int
recogNum :: String -> String -> (Token, String, String)			
recogNum lStr rStr =
	let (row, col) = getRowCol lStr in
		((Num (read (num) :: Int), row, col), lStr ++ take (length num) rStr, drop (length num) rStr)
	where num = lexNumber lStr rStr

-- Helper function to recognise numbers
-- returns String containing longest number possible
-- number consists of digits defined by function firstIsDigit
lexNumber :: String -> String -> String
lexNumber _ [] = []
lexNumber lStr (x:rStr)
	| not (firstIsDigit [x])
		= []
	| otherwise
		= x : lexNumber (lStr ++ [x]) rStr
	  
------------------------------
-- Error Handling Functions --
------------------------------
	  
errorUnexpectedChar lStr x rStr 
	| null rStr
		= error ("\n\nScanner - Unexpected Character:\n\n"	
			     ++ lStr ++  "<<HERE>>" ++ [x] ++ "\n")
	| otherwise
		= error ("\n\nScanner - Unexpected Character:\n\n"	
			     ++ lStr ++ [x] ++ "<<HERE>>" ++ [head rStr] ++ "\n")					

errorInputEndedEarly lStr
	= error ("\n\nScanner - Input ended early:\n\n" 
	         ++ lStr ++ "<<HERE>>" ++ "\n")			

			 
--------------------
-- Misc Functions --
--------------------

getRowCol :: String -> (Int, Int)			 
getRowCol xs = getRowColHelper xs 1 1
			 
getRowColHelper :: String -> Int -> Int -> (Int, Int)			 
getRowColHelper [] col row = (col, row)
getRowColHelper (x:xs) col row
	| x == '\t'
		= getRowColHelper xs col (row+tabSpace)
	| x == '\n'
		= getRowColHelper xs (col+1) 1
	| otherwise
		= getRowColHelper xs col (row+1)
