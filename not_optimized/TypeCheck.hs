module TypeCheck where

import Types

-- Recursively checks types in the whole syntax tree
-- on type error, error message is produced and program stops 
typeCheck :: [Decl] -> Bool
typeCheck decls = typeCheckDecls decls

typeCheckDecls :: [Decl] -> Bool
typeCheckDecls [] = True

typeCheckDecls (d:ds) =
	do
		let b = typeCheckDecl d
		b && typeCheckDecls ds

typeCheckStmts :: [Stmt] -> Bool
typeCheckStmts [] = True

typeCheckStmts (s:ss) =
	do
		let b = typeCheckStmt s
		b && typeCheckStmts ss

-- Checks type of decl
typeCheckDecl :: Decl -> Bool
typeCheckDecl (VarDecl TypeInt _ _ _ _) = True

-- Error produced on declaration of an array with size 0
typeCheckDecl (VarDecl TypeArray id size _ _) = if size /= 0 then True
	else error ("Array variable " ++ show id ++" declared with size 0")

typeCheckDecl (VarDecl TypeParray _ _ _ _) = True

typeCheckDecl (VarDecl TypeVoid id _ _ _) = 
	error ("Variable "++ show id ++" declared with type Void")

typeCheckDecl (FunDecl _ _ decls stmt _) = typeCheckDecls decls && typeCheckStmt stmt

-- Checks the type of stmt
typeCheckStmt :: Stmt -> Bool
typeCheckStmt (ExpStmt exp) = 
	do
		let expType = typeCheckExp exp
		if (expType == TypeInt) || (expType == TypeVoid) then True
			else error ("Type error in expression statement " ++ (findIds exp))

typeCheckStmt (CompStmt decls stmts) = typeCheckDecls decls && typeCheckStmts stmts

-- IfStmt can be with or without else part, error can be reporten only for the expression
-- if error is found in one of the stmts then it it reported there
typeCheckStmt (IfStmt exp stmt1 stmt2) =
	do
		let expType = typeCheckExp exp
		let s1Val = typeCheckStmt stmt1
		if (stmt2 /= NullStmt) 
			then do
				let s2Val = typeCheckStmt stmt2
				if expType == TypeInt && s1Val == True && s2Val == True
					then True
					else error ("Type error in expression " ++ (findIds exp) ++ " in selection statement")
			else do
				if expType == TypeInt && s1Val == True 
					then True
					else error ("Type error in expression " ++ (findIds exp) ++ " in selection statement")

typeCheckStmt (WhileStmt exp stmt) =
	if (typeCheckExp exp == TypeInt && typeCheckStmt stmt) 
		then True
		else error ("Type error in while statement " ++ (findIds exp))

typeCheckStmt (ReturnStmt exp)
	| exp == NullExp = True
	| otherwise = if typeCheckExp exp == TypeInt 
		then True
		else error("Type error in return value " ++ (findIds exp))


typeCheckExp :: Exp -> Type
typeCheckExp (AssignExp exp1 exp2) = 
	if  typeCheckExp exp1 == TypeInt && typeCheckExp exp2 == TypeInt 
		then TypeInt
		else error("Type error in assignment of "
				++ (findIds exp2) ++ " to " ++ (findIds exp1))

typeCheckExp (OpExp exp1 op exp2) =
	if typeCheckExp exp1 == TypeInt && typeCheckExp exp2 == TypeInt 
		then TypeInt
		else error("Type error in op node \""
					++ (show op) ++ "\" near identifier(s): "
					++ (findIds exp1) ++" " ++ (findIds exp2))

typeCheckExp (IdExp tok (VarDecl declType _ size _ _)) =
	if (declType == TypeInt && size == 0) 
		then TypeInt
		else if (declType == TypeArray && size > 0) 
				then TypeArray
				else if (declType == TypeParray) 
					then TypeParray
					else error ("Type error in ID expression " ++ (show tok))

typeCheckExp (IdExp tok (FunDecl _ _ _ _ _)) = 
	error ("Type error: " ++ show tok ++ " is not a variable")

typeCheckExp (ArrayExp tok (VarDecl declType _ size _ _) exp) =
	if (((declType == TypeArray && size > 0) || declType == TypeParray) 
				&& (typeCheckExp exp == TypeInt) ) then TypeInt
		else error("Type error in array expression " ++ show tok) 

typeCheckExp (ArrayExp tok (FunDecl _ _ _ _ _) exp) = 
	error ("Type error: " ++ show tok ++ " is not a variable")

typeCheckExp (CallExp tok (FunDecl declRetType _ declParams _ _) exps) =
	if (typeCheckParams declParams exps tok) then declRetType
		else error ("Type error: Invalid number of arguments " ++ show tok)
		
typeCheckExp (CallExp tok (VarDecl _ _ _ _ _) exps) = 
	error ("Type error: " ++ show tok ++ " is not a function")

typeCheckExp (NumExp num) = TypeInt

typeCheckExp (NullExp) = TypeVoid

-- Funcion checks the type of parameters in a call
typeCheckParams :: [Decl] -> [Exp] -> Token -> Bool
typeCheckParams [] [] tok = True
typeCheckParams [] (e:es) tok = error("Type error: Function " ++ show tok ++ " has too many parameters")
typeCheckParams (d:ds) [] tok = error("Type error: Function " ++ show tok ++ " requires more parameters")

-- TypePArray in declaration matches TypaArray and also TypeParray in expression
-- TypeInt can be only matched with the same type
typeCheckParams ((VarDecl declType _ _ _ _):ds) (e:es) tok = 
	if (declType == TypeParray && (expType == TypeParray || expType == TypeArray)) 
		|| (expType == TypeInt && declType == TypeInt) 
		then typeCheckParams ds es tok
		else error("Type error: Function call " ++ show tok ++ " expected type " ++ show declType ++ " got type " ++ show expType ++ " in parameter " ++ findIds e)
		where expType = typeCheckExp e

-- Function returns IDs in subexpressions
findIds :: Exp -> String
findIds (AssignExp e1 e2) = findIds e1 ++ " " ++ findIds e2
findIds (OpExp e1 op e2) = findIds e1 ++ " " ++ findIds e2
findIds (NumExp num) = []
findIds (IdExp tok _) = show tok
findIds (ArrayExp tok _ _) = show tok
findIds (CallExp tok _ _) = show tok
