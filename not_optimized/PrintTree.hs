module PrintTree where

import System.IO

import Types

-- Recursively prints the syntax tree to the output handle
-- with indentation
printDecls :: Handle -> [Decl] -> String -> IO ()
printDecls h [] _ = hPutStr h ""
printDecls h (d:ds) i = 
	do	
		printDecl h d i
		printDecls h ds i
									
printDecl :: Handle -> Decl -> String -> IO ()
printDecl h (VarDecl arrayType id size level offset) i = 
	do
		hPutStr h (i ++ "VarDecl:\n")
		let iNew = (i ++ "\t")
		hPutStr h (iNew ++ "Type: " ++ (show arrayType) ++ "\n")
		hPutStr h (iNew ++ "ID: " ++ (show id) ++ "\n")
		hPutStr h (iNew ++ "Size: " ++ (show size) ++ "\n")
		hPutStr h (iNew ++ "Level: " ++ (show level) ++ "\n")
		hPutStr h (iNew ++ "Offset: " ++ (show offset) ++ "\n")

printDecl h (FunDecl returnType id declList compStmt address) i =
	do 
		hPutStr h (i ++ "FunDecl:\n")
		let iNew = (i ++ "\t")
		hPutStr h (iNew ++ "Type: " ++ (show returnType) ++ "\n")
		hPutStr h (iNew ++ "ID: " ++ (show id) ++ "\n")
		hPutStr h (iNew ++ "Address: " ++ (show address) ++ "\n")
		hPutStr h (iNew ++ "Params: " ++ if declList == [] then " \n" else (printDeclListFlat declList) ++ "\n")
		hPutStr h (iNew ++ "Body: " ++ "\n") 
		printStmt h compStmt (iNew ++ "\t")  -- added indentation level

printDecl h (NullDecl) i =
		hPutStr h (i ++ "NullDecl\n")

printStmts :: Handle -> [Stmt] -> String -> IO ()
printStmts h [] _ = hPutStr h ""
printStmts h (s:ss) i =
	do
		printStmt h s i
		printStmts h ss i

printStmt :: Handle -> Stmt -> String -> IO ()
printStmt h (ExpStmt exp) i =
	do
		hPutStr h (i ++ "ExpStmt:\n")
		let iNew = (i ++ "\t")
		hPutStr h (iNew ++ "Exp: " ++ "\n")
		printExp h exp (iNew ++ "\t")

printStmt h (CompStmt declList stmtList) i =
	do
		hPutStr h (i ++ "CompStmt:\n")
		let iNew = (i ++ "\t")
		hPutStr h (iNew ++ "Decls: " ++ "\n")
		printDecls h declList (iNew ++ "\t")
		hPutStr h (iNew ++ "Stmts: " ++ "\n")
		printStmts h stmtList (iNew ++ "\t")
		hPutStr h "\n"

printStmt h (IfStmt exp thenStmt elseStmt) i =
	do
		hPutStr h (i ++ "IfStmt:\n")
		let iNew = (i ++ "\t")
		hPutStr h (iNew ++ "Exp: " ++ "\n")
		printExp h exp (iNew ++ "\t")
		hPutStr h (iNew ++ "ThenStmt: " ++ "\n")
		printStmt h thenStmt (iNew ++ "\t")
		hPutStr h (iNew ++ "ElseStmt: " ++ "\n")
		printStmt h elseStmt (iNew ++ "\t")

printStmt h (WhileStmt exp stmt) i = 
	do 
		hPutStr h (i ++ "WhileStmt:\n")
		let iNew = (i ++ "\t")
		hPutStr h (iNew ++ "Exp: " ++ "\n")
		printExp h exp (iNew ++ "\t")
		hPutStr h (iNew ++ "Stmt" ++ "\n")
		printStmt h stmt (iNew ++ "\t")

printStmt h (ReturnStmt exp) i = 
	do
		hPutStr h (i ++ "ReturnStmt:\n")
		let iNew = (i ++ "\t")
		hPutStr h (iNew ++ "Exp: " ++ "\n")
		printExp h exp (iNew ++ "\t")

printStmt h (NullStmt) i = 
		hPutStr h (i ++ "\t" ++ "NullStmt\n")

printExps :: Handle -> [Exp] -> String -> IO ()
printExps h [] _ = hPutStr h ""
printExps h (e:es) i =
	do
		printExp h e i
		printExps h es i

printExp :: Handle -> Exp -> String -> IO ()
printExp h (AssignExp var exp) i = 
	do 
		hPutStr h (i ++ "AssignExp:\n")
		let iNew = (i ++ "\t")
		hPutStr h (iNew ++ "Variable: " ++ "\n")
		printExp h var (iNew ++ "\t")
		hPutStr h (iNew ++ "Exp: " ++ "\n")
		printExp h exp (iNew ++ "\t")

printExp h (OpExp lExp operator rExp) i = 
	do
		hPutStr h (i ++ "OpExp:\n")
		let iNew = (i ++ "\t")
		hPutStr h (iNew ++ "LExp: " ++ "\n")
		printExp h lExp (iNew ++ "\t")
		hPutStr h (iNew ++ "Operator: " ++ (show operator) ++ "\n")
		hPutStr h (iNew ++ "RExp: " ++ "\n")
		printExp h rExp (iNew ++ "\t")

printExp h (IdExp id decl) i = 
	do
		hPutStr h (i ++ "IdExp:\n")
		let iNew = (i ++ "\t")
		hPutStr h (iNew ++ "ID: " ++ (show id) ++ "\n")
		hPutStr h (iNew ++ "Decl: " ++ show (decl) ++ "\n") 

printExp h (ArrayExp id decl exp) i =
	do
		hPutStr h (i ++ "ArrayExp:\n")
		let iNew = (i ++ "\t")
		hPutStr h (iNew ++ "ID: " ++ (show id) ++ "\n")
		hPutStr h (iNew ++ "Decl: " ++ show (decl) ++ "\n") 
		hPutStr h (iNew ++ "Exp: " ++ "\n")
		printExp h exp (iNew ++ "\t")

printExp h (CallExp id decl expList) i =
	do
		hPutStr h (i ++ "CallExp:\n")
		let iNew = (i ++ "\t")
		hPutStr h (iNew ++ "ID: " ++ (show id) ++ "\n")
		hPutStr h (iNew ++ "Decl: " ++ show (decl) ++ "\n")
		hPutStr h (iNew ++ "Exp: " ++ "\n")
		printExps h expList (iNew ++ "\t")

printExp h (NumExp num) i =
	do
		hPutStr h (i ++ "NumExp:\n")
		let iNew = (i ++ "\t")
		hPutStr h (iNew ++ (show (Num num)) ++ "\n")

printExp h (NullExp) i =
	hPutStr h (i ++ "NullExp\n")