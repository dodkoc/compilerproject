module Constants where

-- Printing

tabSpace = (4 :: Int)

-- Code generation

initFunLabel = (2 :: Int)

initGP = (0 :: Int) 

ofpFO = (0 :: Int)
retFO = (-1 :: Int)
initFO = (-2 :: Int)

pc = (7 :: Int)
gp = (6 :: Int)
fp = (5 :: Int)
ac = (0 :: Int)
ac1 = (1 :: Int)
r2 = (2 :: Int)
r3 = (3 :: Int)
r4 = (4 :: Int)