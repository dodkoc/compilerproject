module SymTab where

import Types
import Constants
	
-- Initialize	
	
stInitial :: SymbolTable
stInitial = [(initFunLabel,initGP,
				[(FunDecl TypeInt (Id "input",0,0) [] NullStmt 0),
                 (FunDecl TypeVoid (Id "output",0,0) 
					[(VarDecl TypeInt (Id "x",0,0)) 0 Local initFO] 
			      NullStmt 1)
				]
			 )
			]

			
-- All functions may assume symbol table with at least 1 open scope with at least 1 function present			
-- Insert
-- Function inserts the declaration into the existing Symbol table
-- If the current scope of symbol table already contains an entry with the same name,
-- declaration from the Symbol table and empty list are returned meaning error.
-- Otherwise the new symbol is inserted into the table and it's declaration
-- with updated table are returned			
stInsert :: Decl -> SymbolTable -> (Decl, SymbolTable)
stInsert (VarDecl varType id size level offset) ((fc, oc, decl):ts)
	| found /= Nothing = (liftJust found, [])
	| size == 0 = (d, ((fc, oc-1, d:decl):ts))
	| otherwise = (d, ((fc, oc-size, d:decl):ts))
	where
		found = stLookupInCurrentScope (getDeclName d) decl
		d = (VarDecl varType id size level oc)

-- Insertion into the symbol table also updates the addres in funcion declaration
stInsert (FunDecl returnType id declList compStmt address) ((fc, oc, decl):ts) 
	| found /= Nothing = (liftJust found, [])
	| otherwise = ((FunDecl returnType id declList compStmt fc), (fc+1, oc, d:decl):ts)
	where 
		found = stLookupInCurrentScope (getDeclName d) decl
		d = (FunDecl returnType id declList NullStmt fc)

-- Lookup				
-- one scope is a list of declarations
-- recursively goes through the scope until the end or until the name matches a Decl in the list
stLookupInCurrentScope :: String -> [Decl] -> Maybe Decl
stLookupInCurrentScope name [] = Nothing
stLookupInCurrentScope name (d:ds) = 
	if name == getDeclName d then Just d
		else 
			stLookupInCurrentScope name ds	

-- searches in the symbol table using stLookupInCurrentScope
stLookup :: String -> SymbolTable -> Maybe Decl
stLookup name [] = Nothing
stLookup name (t:ts) = 
	do 
		let (_, _, scope) = t
		let decl = stLookupInCurrentScope name scope
		if decl == Nothing then
			stLookup name ts
			else
				decl

				
-- Scope operations with new offset
-- adds new scope for a function wih initial offset
stBeginScopeNewOffset :: SymbolTable -> SymbolTable
stBeginScopeNewOffset (t:ts) = ((fc, initFO, []):t:ts)
	where (fc, _, _) = t

-- removes the scope of the function
stEndScopeNewOffset :: SymbolTable -> SymbolTable	
stEndScopeNewOffset t = tail t

-- Scope operations with same offset	
-- adds new scope for with the same offset as previous scope
stBeginScope :: SymbolTable -> SymbolTable	
stBeginScope (t:ts) = ((fc, oc, []):t:ts)
	where (fc, oc, _) = t

--removes the scope of the compound-statement
stEndScope :: SymbolTable -> SymbolTable	
stEndScope ((_, oc1, _):(fc2, _, decl2):ts) = ((fc2, oc1, decl2):ts)
	

	