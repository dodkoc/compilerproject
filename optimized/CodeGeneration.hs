module CodeGeneration where

import Types
import Constants

-- Code Generation Helpers	
-- Returned code is with one optimization, three remaining registers: 2,3,4 are
-- used for temporary veriables, if they are not available for the current
-- expression, memory is used instead

-- decls
-- Parameters are list of declarations, Function Table, number of instructions already
-- generated, output is the list of generated insturctions and updated funTable	
cGenDecls :: [Decl] -> FunTable -> Int -> ([Instruction], FunTable)
cGenDecls [] funTable _ = ([], funTable)
cGenDecls (d:ds) funTable size = 
	do
		let (code, funTable1) = cGenDecl d funTable size 
		let (code1, funTable2) = cGenDecls ds funTable1 ((length code) + size)
		(code ++ code1, funTable2)

-- decl
-- FreeOffset for the stmt code is passed on as a positive number
-- and it's location after function parameters and local variables
-- Register 2 is passed on to cGenStmt functinon, so that every function
-- has 3 temporary registers available at the beginning
cGenDecl :: Decl -> FunTable -> Int -> ([Instruction], FunTable)  		
cGenDecl (FunDecl _ id params stmt funLabel) funTable size = 
	do
		let funTable1 = funTable ++ [(funLabel, size)] -- (function label, address)
		let stmtCode = cGenStmt stmt funTable1 (2+(findSizeOfParamsAndLocalVarsInDecls params)+ findSizeOfParamsAndLocalVarsInStmt stmt) r2
		let code = [(RM ST ac retFO fp ("Begin function definition " ++ show id ++ ", store return address"))]
		let retCode = [(RM LD pc retFO fp "return to caller")]
		(code ++ stmtCode ++ retCode, funTable1)

cGenDecl (VarDecl _ _ _ _ _) funTable _ = ([], funTable) -- variable declaration generates no code

-- stmts		

cGenStmts :: [Stmt] -> FunTable -> Int -> Int -> [Instruction]
cGenStmts [] _ _ _ = []
cGenStmts (s:ss) funTable offset register = 
	do
		let stmtCode = cGenStmt s funTable offset register
		let stmtsCode = cGenStmts ss funTable offset register
		stmtCode ++ stmtsCode
	    
-- stmt		
-- Parameters are Statement, Function Table, Free Offset and Free Register 
-- Retruns the code for each statement	
cGenStmt :: Stmt -> FunTable -> Int -> Int -> [Instruction]
cGenStmt (ExpStmt exp) funTable offset register = cGenExp exp funTable offset register

cGenStmt (CompStmt decls stmts) funTable offset register= 
	do
		let stmtsCode = cGenStmts stmts funTable offset register
		stmtsCode

cGenStmt (IfStmt exp thenStmt elseStmt) funTable offset register = 
	do
		let expCode = cGenExp exp funTable offset register
		let thenStmtCode = cGenStmt thenStmt funTable offset register
		let elseStmtCode = cGenStmt elseStmt funTable offset register
		expCode ++ 
			[(RM JEQ ac ((length thenStmtCode)+1) pc "jump to else part on 0")] ++
			thenStmtCode ++
			[(RM LDA pc (length elseStmtCode) pc "jump over else part")] ++
			elseStmtCode

cGenStmt (WhileStmt exp stmt) funTable offset register=
	do
		let expCode = cGenExp exp funTable offset register
		let stmtCode = cGenStmt stmt funTable offset register
		expCode ++ 
			[(RM JEQ ac ((length stmtCode)+1) pc "finish loop")] ++
			stmtCode ++
			[(RM LDA pc (-((length stmtCode) + (length expCode) + 2)) pc "return to loop expression check")]

cGenStmt (ReturnStmt exp) funTable offset register = 
	do
		let expCode = cGenExp exp funTable offset register
		expCode ++
			[(RM LD pc retFO fp "explicit return to caller")]


cGenStmt (NullStmt) _ _ _ = []

-- exp	
-- Parameters are Exp, Function Table, Free Offset, Free Register
-- Retruns the code for each expression
-- Value of the free register number is tested
-- if it is available then it is used
-- otherwise temporary area memory is used 
cGenExp :: Exp -> FunTable -> Int -> Int -> [Instruction]
cGenExp (NumExp num) _ _ _= [(RM LDC ac num 0 "load numeric constant")]

-- id can be either global or local only
cGenExp (IdExp tok decl@(VarDecl _ _ _ idLevel idOffset)) funTable offset register
	| idLevel == Local = [(RM LD 0 idOffset fp ("load local id value " ++ show tok))]
	| otherwise = [(RM LD 0 idOffset gp ("load global id value " ++ show tok))]

cGenExp (AssignExp lhs rhs) funTable offset register= 
	do
		let lhsCode = cGenIdAddress lhs funTable offset register
		let tempStoreCode = if register >= r2 && register <= r4 
								then [(RM LDC register 0 0 "load 0 to temp register"),
									  (RO ADD register register ac "store address in register" )]
								else [(RM ST ac (-offset) fp "store address in temporary area")]
		let rhsCode = if register >= r2 && register <= r4 
						then cGenExp rhs funTable offset (register+1)
						else cGenExp rhs funTable (offset+1) register
		let assignCode = if register >= r2 && register <= r4
							then [(RM ST ac 0 register "store value to address in temp register" )]
							else [(RM LD ac1 (-offset) fp "load id address to ac1"),
						  		  (RM ST ac 0 ac1 "store value of assignment to address")]
		lhsCode ++ tempStoreCode ++ rhsCode ++ assignCode

-- specific code for each operator
-- arithmetic operations work only with registers
-- boolean operators are implemented via subtraction and then conditional jump
cGenExp (OpExp lhs op rhs) funTable offset register
	| op == PlusOp = do
		let plusCode = if register >= r2 && register <= r4
						then [(RO ADD ac register ac "addition operation on temp reg")]
						else [(RO ADD ac ac1 ac "addition operation")]
		lhsCode ++ tempStoreValCode ++ rhsCode ++ opCode ++ plusCode
	| op == SubtractOp = do
		let subtractCode = if register >= r2 && register <= r4
							then [(RO SUB ac register ac "subtraction operation on temp reg")]
							else [(RO SUB ac ac1 ac "subtraction operation")]
		lhsCode ++ tempStoreValCode ++ rhsCode ++ opCode ++ subtractCode
	| op == MultOp = do
		let multCode = if register >= r2 && register <= r4
							then [(RO MUL ac register ac "multiplication operation on temp reg")]
							else [(RO MUL ac ac1 ac "multiplication operation")]
		lhsCode ++ tempStoreValCode ++ rhsCode ++ opCode ++ multCode
	| op == DivOp = do
		let divCode = if register >= r2 && register <= r4
							then [(RO DIV ac register ac "division operation")]
							else [(RO DIV ac ac1 ac "division operation")]
		lhsCode ++ tempStoreValCode ++ rhsCode ++ opCode ++ divCode
	| op == NeqOp = do
		let testCode = [(RM JNE ac 2 pc "jump to set True on NeqOp")]
		lhsCode ++ tempStoreValCode ++ rhsCode ++ opCode ++ 
			boolBeginCode ++ testCode ++ boolEndCode
	| op == EqOp = do
		let testCode = [(RM JEQ ac 2 pc "jump to set True on EqOp")]
		lhsCode ++ tempStoreValCode ++ rhsCode ++ opCode ++ 
			boolBeginCode ++ testCode ++ boolEndCode
	| op == GeOp = do
		let testCode = [(RM JGE ac 2 pc "jump to set True on GeOp")]
		lhsCode ++ tempStoreValCode ++ rhsCode ++ opCode ++ 
			boolBeginCode ++ testCode ++ boolEndCode
	| op == LeOp = do
		let testCode = [(RM JLE ac 2 pc "jump to set True on LeOp")]
		lhsCode ++ tempStoreValCode ++ rhsCode ++ opCode ++ 
			boolBeginCode ++ testCode ++ boolEndCode
	| op == GtOp = do
		let testCode = [(RM JGT ac 2 pc "jump to set True on GtOp")]
		lhsCode ++ tempStoreValCode ++ rhsCode ++ opCode ++ 
			boolBeginCode ++ testCode ++ boolEndCode
	| op == LtOp = do
		let testCode = [(RM JLT ac 2 pc "jump to set True on LtOp")]
		lhsCode ++ tempStoreValCode ++ rhsCode ++ opCode ++ 
			boolBeginCode ++ testCode ++ boolEndCode
		where
			lhsCode = cGenExp lhs funTable offset register
			tempStoreValCode = if register >= r2 && register <= r4 
								then [(RM LDC register 0 0 "load 0 to temp register"),
									  (RO ADD register register ac "store value in temp register" )]
								else [(RM ST ac (-offset) fp "store value in temporary area")]
			rhsCode = if register >= r2 && register <= r4 
						then cGenExp rhs funTable offset (register+1)
						else cGenExp rhs funTable (offset+1) register
			
			opCode = if register >= r2 && register <= r4
						then []
						else [(RM LD ac1 (-offset) fp "load stored value to ac1")]
			
			boolBeginCode = if register >= r2 && register <= r4
								then [(RO SUB ac register ac "subtraction operation on temp register")]
								else [(RO SUB ac ac1 ac "subtraction operation")]
			boolEndCode = [(RM LDC ac 0 0 "set False"),
					  	(RM LDA pc 1 pc "jump over next instruction"),
					  	(RM LDC ac 1 0 "set True")]


cGenExp (ArrayExp tok decl subs) funTable offset register = 
	do
		let addrCode = cGenIdAddress (ArrayExp tok decl subs) funTable offset register
		let arrayCode = [(RM LD ac 0 ac "load value of array node")]
		addrCode ++ arrayCode 

-- Values from the temporary registers must be stored in the temporary area before call
-- after the call they are loaded back to the previous registers
cGenExp (CallExp tok decl@(FunDecl _ _ _ _ label) args) funTable offset register = 
	do
		let (offset1, register1, storeTemRegCode) = cGenStoreTempReg offset register
		let loadTemRegCode = cGenLoadTempReg offset1 register1 register
		let argsCode = cGenArgs args funTable (offset1-initFO) register
		let funAddress = liftJust(lookup label funTable)
		let callCode = [(RM LDC ac1 0 0 "load constant 0 to ac1"),
						(RM ST fp (-offset1-ofpFO) fp "save current fp at ofpFO"),
						(RM LDA fp (-offset1) fp "set new frame"),
						(RM LDA ac 1 pc "load ac with return address"),
						(RM LDA pc (funAddress) ac1 ("jump to function entry" ++ show tok)),
						(RM LD fp ofpFO fp "pop current frame")]
		
		storeTemRegCode ++ argsCode ++ callCode ++ loadTemRegCode

cGenExp (NullExp) _ _ _ = []


-- Parameters are offset and register number
-- Outputs tuple containing new offset, register number and instructions
-- register number 3 means that 1 temporary register is currently in use
-- function decreases the register number in subsequent calls and increases offset
cGenStoreTempReg :: Int -> Int -> (Int, Int, [Instruction])
cGenStoreTempReg offset register 
	| register >= 3 = do
		let code = [(RM ST (register-1) (-offset) fp "store value of temp register to temporary area")]
		let (newOffset, newRegister, code2) = cGenStoreTempReg (offset+1) (register-1)
		(newOffset, newRegister, code ++ code2)
	| otherwise = (offset, register, [])

-- Parameters are offset, register, maximal used register, code
-- Returns only instructions
-- Uses the values returned from the cGenStoreTempReg to know the returned free offset
-- subsequent calls increase register number and decrease offset so values are loaded
-- in the same order as they were stored 
cGenLoadTempReg :: Int -> Int -> Int -> [Instruction]
cGenLoadTempReg offset register maxReg
	| register < maxReg = do
		let code2 = cGenLoadTempReg (offset-1) (register+1) maxReg
		let code = [(RM LD (register) (-offset+1) fp "load value from temporary area to temp register")]
		code ++ code2
	| otherwise = []

-- Parameters are Exp, Function Table, Free Offset, Free Register
-- Generates code for computing the arguments
-- Free register is used only for generation code of expressions,
-- because values of function call arguments are always stored in the memory
cGenArgs :: [Exp] -> FunTable -> Int -> Int -> [Instruction]
cGenArgs [] _ _ _ = []

-- Specific case of ID expression, for Array and Parray the address needs to be stored
-- For any other expression value is stored
cGenArgs ((IdExp tok decl@(VarDecl declType _ _ _ _)):es) funTable offset register
	| (declType == TypeParray) || (declType == TypeArray) = do
		let eCode = cGenIdAddress (IdExp tok decl) funTable offset register
		eCode ++ argValCode ++ esCode
	| otherwise = do
		let eCode = cGenExp (IdExp tok decl) funTable offset register
		eCode ++ argValCode ++ esCode
		where
			argValCode = [(RM ST ac (-offset) fp "store arg value")]
			esCode = cGenArgs es funTable (offset+1) register
	
cGenArgs (e:es) funTable offset register =
	do
		let eCode = cGenExp e funTable offset register
		let argValCode = [(RM ST ac (-offset) fp "store arg value")]
		let esCode = cGenArgs es funTable (offset+1) register
		eCode ++ argValCode ++ esCode

-- Parameters are Exp, Function Table, Free Offset, Free Register
-- Free register is used only for generation code of expressions
-- Function generates the code for ID address used for array operation and call arguments
cGenIdAddress :: Exp -> FunTable -> Int -> Int -> [Instruction]

-- Array is treated as normal local/global variable for which base address is saved,
-- In case of Parray, the address of array is stored in the address of local variable
cGenIdAddress (IdExp tok decl@(VarDecl idType _ _ idLevel idOffset)) funTable offset register
	| idType == TypeParray = [(RM LD ac idOffset fp ("load address of passed parray " ++ show tok))]
	| idLevel == Local = [(RM LDA ac idOffset fp ("load local id address " ++ show tok))]
	| otherwise =  [(RM LDA ac idOffset gp ("load global id address " ++ show tok))]

-- Array expression first computes the value of expression in the subscript
-- bund check only checks if subscript is not less than 0
-- in Parray, adress of the actual array is stored in the local variable
cGenIdAddress (ArrayExp tok decl@(VarDecl idType _ idSize idLevel idOffset) exp) funTable offset register
	| (idType == TypeArray) && (idLevel == Local) = do
		let baseAddressCode = [(RM LDA ac1 idOffset fp ("load base of local array " ++ show tok))]
		expCode ++ boundCheckCode ++ baseAddressCode ++ reqElemCode
	| (idType == TypeArray) && (idLevel == Global) = do
		let baseAddressCode = [(RM LDA ac1 idOffset gp ("load base of global array " ++ show tok))]
		expCode ++ boundCheckCode ++ baseAddressCode ++ reqElemCode
	| otherwise = do
		let baseAddressCode = [(RM LD ac1 idOffset fp ("load base of parray " ++ show tok))]
		expCode ++ boundCheckCode ++ baseAddressCode ++ reqElemCode
		where
			expCode = cGenExp exp funTable offset register
			boundCheckCode = [(RM JGE ac 1 pc "jump over halt on subscript >= 0"),
							  (RO HALT 0 0 0 "halt on negative array subscript")] 
			reqElemCode = [(RO SUB ac ac1 ac "apply subscript to base address")]


-- Main Code Generation Function	

codeGen :: SyntaxTree -> [Instruction]
codeGen syntaxTree = 
	do
		let funTable = [(0,sizeOfPreludeCode+5), (1, sizeOfPreludeCode+5+sizeOfInputFunCode)]
		let instructionCount = sizeOfPreludeCode + sizeOfInputFunCode + sizeOfOutputFunCode + 5
		let (instructions, retFunTable) = cGenDecls syntaxTree funTable instructionCount
		let (_,mainAddr) = last retFunTable
		let globalArea = findSizeOfGlobalArea syntaxTree
		let setFrame = [(RM LDA fp (-globalArea) fp "set new frame")]
		let callMain = [(RM LDC ac1 0 0 "load constant 0 to ac1"),
					(RM LDA ac 1 pc "load ac with return address from 'main'"),
					(RM LDA pc mainAddr ac1 "jump to function 'main'"),
					(RO HALT 0 0 0 "")]

		preludeCode ++ setFrame ++ callMain ++ inputFunCode ++ outputFunCode ++ instructions
	
-- Size helpers	
	
findSizeOfGlobalArea :: [Decl] -> Int
findSizeOfGlobalArea [] = 0
findSizeOfGlobalArea ((FunDecl _ _ _ _ _):ds) = findSizeOfGlobalArea ds
findSizeOfGlobalArea ((VarDecl _ _ size _ _ ):ds) 
	| size == 0 = 1 + (findSizeOfGlobalArea ds)
	| otherwise = size + (findSizeOfGlobalArea ds)

findSizeOfParamsAndLocalVarsInDecl :: Decl -> Int
findSizeOfParamsAndLocalVarsInDecl (FunDecl _ _ decls stmt _ ) =
	(findSizeOfParamsAndLocalVarsInDecls decls) + (findSizeOfParamsAndLocalVarsInStmt stmt)

findSizeOfParamsAndLocalVarsInDecl (VarDecl _ _ size _ _) = 
	if (size == 0) then 1
		else size

findSizeOfParamsAndLocalVarsInDecls :: [Decl] -> Int
findSizeOfParamsAndLocalVarsInDecls [] = 0
findSizeOfParamsAndLocalVarsInDecls (d:ds) =
	do 
		let dSize = findSizeOfParamsAndLocalVarsInDecl d
		let dsSize = findSizeOfParamsAndLocalVarsInDecls ds 
		dSize + dsSize

findSizeOfParamsAndLocalVarsInStmt :: Stmt -> Int
findSizeOfParamsAndLocalVarsInStmt (CompStmt decls stmts) = 
	(findSizeOfParamsAndLocalVarsInDecls decls) + (findSizeOfParamsAndLocalVarsInStmts stmts)

findSizeOfParamsAndLocalVarsInStmt (IfStmt _ stmt1 stmt2) =
	(findSizeOfParamsAndLocalVarsInStmt stmt1) + (findSizeOfParamsAndLocalVarsInStmt stmt2)

findSizeOfParamsAndLocalVarsInStmt (WhileStmt _ stmt) = 
	findSizeOfParamsAndLocalVarsInStmt stmt 

findSizeOfParamsAndLocalVarsInStmt _ = 0

findSizeOfParamsAndLocalVarsInStmts :: [Stmt] -> Int
findSizeOfParamsAndLocalVarsInStmts [] = 0
findSizeOfParamsAndLocalVarsInStmts (s:ss) = 
	do
		let sSize = findSizeOfParamsAndLocalVarsInStmt s
		let ssSize = findSizeOfParamsAndLocalVarsInStmts ss 
		sSize + ssSize


-- Static code	
	
preludeCode = 
	[(RM LD gp 0 ac "load gp with maxaddress"),
	 (RM LDA fp 0 gp "copy gp to fp"),
	 (RM ST ac 0 ac "clear location 0")]
sizeOfPreludeCode = length preludeCode
	 
outputFunCode =
	[(RM ST ac retFO fp "Begin built-in function definition 'output', store return address"),
	 (RM LD ac initFO fp "load output value"),
	 (RO OUT ac 0 0 "output"),
	 (RM LD pc retFO fp "End built-in function definition 'output', return to caller")]
sizeOfOutputFunCode = length outputFunCode

inputFunCode = 
	[(RM ST ac retFO fp "Begin built-in function definition 'input', store return address"),
	 (RO IN ac 0 0 "input"),
	 (RM LD pc retFO fp "End built-in function definition 'input', return to caller")]
sizeOfInputFunCode = length inputFunCode







		
		
		
		
