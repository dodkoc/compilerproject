module Parse where

import Types

-- program
-- Recursive descent parser is written according to the grammar on page 492.
-- All the functions are very similar, and correspond to the grammar rules.
-- The only difference is in additive-expression
-- Tokens in the rules are matched and functions are called for any substatements.
-- Syntax tree with missing declarations for identifiers is returned.

parseProgram :: [Token] -> SyntaxTree
parseProgram ts = syntaxTree where
	(syntaxTree,_) = parseDeclarationList ts

-- declaration-list
	
parseDeclarationList :: [Token] -> ([Decl], [Token])	
parseDeclarationList [] = error "No EOF token!"
parseDeclarationList ((EOF,_,_):[]) = ([], [])
parseDeclarationList ts = 
	do
		let (d, ts1) = parseDeclaration ts
		let (ds, ts2) = parseDeclarationList ts1
		([d] ++ ds, ts2)

-- declaration		
		
parseDeclaration :: [Token] -> (Decl, [Token])		
parseDeclaration (t1:t2:(Semi,r,c):ts) = parseVarDeclaration (t1:t2:(Semi,r,c):ts) Global
parseDeclaration (t1:t2:(Lbracket,r,c):ts) = parseVarDeclaration (t1:t2:(Lbracket,r,c):ts) Global
parseDeclaration (t1:t2:(Lparen,r,c):ts) = parseFunDeclaration (t1:t2:(Lparen,r,c):ts)
parseDeclaration ts = syntaxError ts "declaration"

-- var-declaration
	 
parseVarDeclaration :: [Token] -> Level -> (Decl, [Token])		
parseVarDeclaration ((Int,_,_):(Id str,r,c):(Semi,_,_):ts) level= 
	((VarDecl TypeInt (Id str,r,c) 0 level 0), ts) 
		
parseVarDeclaration ((Int,_,_):(Id str,r,c):(Lbracket,_,_):(Num size,_,_):(Rbracket,_,_):(Semi,_,_):ts) level = 
	((VarDecl TypeArray (Id str,r,c) size level 0), ts)
parseVarDeclaration ts _ = syntaxError ts "var-declaration"

-- type-specifier
-- Two possible types, Void is used only with functions

parseTypeSpecifier :: Token -> Type	
parseTypeSpecifier (Int,_,_) = TypeInt		
parseTypeSpecifier (Void,_,_) = TypeVoid
parseTypeSpecifier t = syntaxError [t] "type-specifier"

-- fun-declaration

parseFunDeclaration :: [Token] -> (Decl, [Token])
parseFunDeclaration (t1:(Id str, l, c):(Lparen,_,_):ts) =
	do
		let declType = parseTypeSpecifier t1
		let (params, ts1) = parseParams ts
		let ts2 = match Rparen ts1 "fun-declaration"
		let (compStmt, ts3) = parseCompoundStmt ts2
		((FunDecl declType (Id str, l, c) params compStmt 0), ts3)
parseFunDeclaration ts = syntaxError ts "fun-declaration"

-- params
-- If function has no parameters, empty list is returned
-- keeps Rparen in the token list to be matched by the calling function
parseParams :: [Token] -> ([Decl], [Token])
parseParams ((Void, _, _):(Rparen, l, c):ts) = ([], (Rparen, l, c):ts)
parseParams ((Rparen, l, c):ts) = ([], (Rparen, l, c):ts)
parseParams ts = parseParamList ts

-- param-list
-- Returns list of declarations in parameter list 

parseParamList :: [Token] -> ([Decl], [Token])
parseParamList ((Rparen, l, c):ts) = ([], (Rparen, l, c):ts)
parseParamList ((Comma, l, c):ts) = syntaxError ((Comma, l, c):ts) "param-list"
parseParamList ts =
	do
		let (p, (t, l, c):ts1) = parseParam ts
		let (ps, ts2) = if t == Comma 
							then parseParamList ts1 
							else ([], (t, l, c):ts1)
		([p] ++ ps, ts2)

-- param
-- Type specifier is always int because it is the only type allowed for variables
parseParam :: [Token] -> (Decl, [Token])
parseParam ((Int, _, _):(Id str,l,c):(Lbracket,_,_):(Rbracket, _, _):ts) =
	((VarDecl TypeParray (Id str, l, c) 0 Local 0), ts)
	-- where declType = parseTypeSpecifier t1
parseParam ((Int, _, _):(Id str, l, c):ts) = ((VarDecl TypeInt (Id str, l, c) 0 Local 0), ts)
	-- where declType = parseTypeSpecifier t1

parseParam ts = syntaxError ts "param"

-- compound-stmt
-- Does not allow local declarations to be present after statements

parseCompoundStmt :: [Token] -> (Stmt, [Token])
parseCompoundStmt ((Lbrace, _, _):ts) = 
	do
		let (d, ts1) = parseLocalDeclarations ts
		let (s, ts2) = parseStatementList ts1
		let ts3 = match Rbrace ts2 "compound-stmt"
		((CompStmt d s), ts3)

-- local-declarations 
-- Empty list is returned if no local declaration is present
parseLocalDeclarations :: [Token] -> ([Decl], [Token])
parseLocalDeclarations ((t, l, c):ts)
	| elem t [Int, Void] = 
		do
			let (d, ts1) = parseVarDeclaration ((t, l, c):ts) Local
			let (ds, ts2) = parseLocalDeclarations ts1
			([d] ++ ds, ts2)

parseLocalDeclarations ts = ([], ts)

-- statement-list

parseStatementList :: [Token] -> ([Stmt], [Token])
parseStatementList ((Rbrace, l, c):ts) = ([], ((Rbrace, l, c):ts))
parseStatementList ts =
	do 
		let (s, ts1) = parseStatement ts
		let (ss, ts2) = parseStatementList ts1
		([s] ++ ss, ts2)

-- statement

parseStatement :: [Token] -> (Stmt, [Token])
parseStatement ((tokClass, l, c):ts)
	| tokClass == Lbrace
		= parseCompoundStmt ((tokClass, l, c):ts)
	| tokClass == If
		= parseSelectionStmt ((tokClass, l, c):ts)
	| tokClass == While 										
		= parseIterationStmt ((tokClass, l, c):ts)			
	| tokClass == Return
		= parseReturnStmt ((tokClass, l, c):ts)
	| otherwise
		= parseExpressionStmt ((tokClass, l, c):ts)

-- expression-stmt

parseExpressionStmt :: [Token] -> (Stmt, [Token])
parseExpressionStmt ((Semi, _, _):ts) = (ExpStmt NullExp, ts)
parseExpressionStmt ts = 
	do
		let (e, ts1) = parseExpression ts
		let ts2 = match Semi ts1 "expression-stmt"
		((ExpStmt e), ts2)

-- selection-stmt

parseSelectionStmt :: [Token] -> (Stmt, [Token])
parseSelectionStmt ((If, _, _):(Lparen, _, _):ts) = 
	do
		let (e, ts1) = parseExpression ts
		let ts2 = match Rparen ts1 "selection-stmt"
		let (s1, ((t, l, c):ts3)) = parseStatement ts2
		let (s2, ts4) = if t == Else 
							then parseStatement ts3 
							else (NullStmt, ((t, l, c):ts3))
		((IfStmt e s1 s2), ts4)

-- iteration-stmt

parseIterationStmt :: [Token] -> (Stmt, [Token])
parseIterationStmt ((While, _, _):(Lparen, _, _):ts) =
	do
		let (e, ts1) = parseExpression ts
		let ts2 = match Rparen ts1 "iteration-stmt"
		let (s, ts3) = parseStatement ts2
		((WhileStmt e s), ts3)

-- return-stmt

parseReturnStmt :: [Token] -> (Stmt, [Token])
parseReturnStmt ((Return, _, _):(Semi, _, _):ts) = ((ReturnStmt NullExp), ts)
parseReturnStmt ((Return, _, _):ts) =
	do
		let (e, ts1) = parseExpression ts
		let ts2 = match Semi ts1 "return-stmt"
		((ReturnStmt e), ts2)

-- expression

parseExpression :: [Token] -> (Exp, [Token])
parseExpression ((Id str, l, c):ts) = 
	do
		let (e1, (t, l1, c1):ts1) = parseVar ((Id str, l, c):ts)
		if t == Assign 
			then do
				let ts2 = match Assign ((t, l1, c1):ts1) "expression"
				let (e2, ts3) = parseExpression ts2
				((AssignExp e1 e2), ts3)
			else parseSimpleExpression ((Id str, l, c):ts)

parseExpression ts = parseSimpleExpression ts

-- var

parseVar :: [Token] -> (Exp, [Token])
parseVar ((Id str, l, c):(Lbracket, _, _):ts) = 
	do
		let (e, ts1) = parseExpression ts
		let ts2 = match Rbracket ts1 "var"
		((ArrayExp (Id str, l, c) NullDecl e), ts2) 	-- NullDecl used only as a temporary placeholder
parseVar ((Id str, l, c):ts) = (IdExp (Id str, l, c) NullDecl, ts)
parseVar ts = syntaxError ts "var"

-- simple-expression
-- Always matches the first part of the simple expression
-- second part is matched only if the first one is followed by operator
parseSimpleExpression :: [Token] -> (Exp, [Token])
parseSimpleExpression ts =
	do
		let (e1, ((t, l, c):ts1)) = parseAdditiveExpression ts
		if (elem t [Lt, Le, Gt, Ge, Eq, Neq]) 
			then do
				let (op, ts2) = parseRelOp ((t, l, c):ts1)
				let (e2, ts3) = parseAdditiveExpression ts2
				((OpExp e1 op e2), ts3)
			else  (e1, ((t, l, c):ts1))

-- relop

parseRelOp :: [Token] -> (Op, [Token])
parseRelOp ((t, l, c):ts)
	| t == Lt = (LtOp, ts)
	| t == Le = (LeOp, ts)
	| t == Gt = (GtOp, ts)
	| t == Ge = (GeOp, ts)
	| t == Eq = (EqOp, ts)
	| t == Neq = (NeqOp, ts)
	| otherwise 
		= syntaxError ((t, l, c):ts) "relop"

-- additive-expression
-- Left-recursion was removed, so new function was added 
parseAdditiveExpression :: [Token] -> (Exp, [Token])
parseAdditiveExpression ts = (e, ts2) where
	(e1, ts1) = parseTerm ts
	(e, ts2) = parseAdditiveExpression' e1 ts1

-- parseAdditiveExpression' builds the whole Exp recursively, with left-associativity
-- additional parameter of type Exp is necessary in order to pass on the part of
-- expression that's already been consturcted to the recursive call

parseAdditiveExpression' :: Exp -> [Token] -> (Exp, [Token])
parseAdditiveExpression' e ((t, l, c):ts)
	| (elem t [Plus, Subtract]) = do
		let (op, ts1) = parseAddOp ((t, l, c):ts)
		let (e2, ts2) = parseTerm ts1
		parseAdditiveExpression' (OpExp e op e2) ts2
	| otherwise =
		(e, (t, l, c):ts)

-- addop

parseAddOp :: [Token] -> (Op, [Token])
parseAddOp ((t, l, c):ts)
	| t == Plus = (PlusOp, ts)
	| t == Subtract = (SubtractOp, ts) 
	| otherwise 
		= syntaxError ((t, l, c):ts) "addop"

-- term
-- the structure is the same as in additive-expression

parseTerm :: [Token] -> (Exp, [Token])
parseTerm ts = (e, ts2) where
	(e1, ts1) = parseFactor ts
	(e, ts2) = parseTerm' e1 ts1

parseTerm' :: Exp -> [Token] -> (Exp, [Token])
parseTerm' e ((t, l, c):ts)
	| (elem t [Mult, Div]) = do
		let (op, ts1) = parseMulop ((t, l, c):ts)
		let (e2, ts2) = parseFactor ts1
		parseTerm' (OpExp e op e2) ts2
	| otherwise = 
		(e, (t, l, c):ts)  

-- mulop

parseMulop :: [Token] -> (Op, [Token])
parseMulop ((t, l, c):ts) 
	| t == Mult = (MultOp, ts) 
	| t == Div = (DivOp, ts)
	| otherwise 
		= syntaxError ((t, l, c):ts) "mulop"

-- factor
parseFactor :: [Token] -> (Exp, [Token])
parseFactor ((Lparen, _, _):ts) =
	do
		let (e, ts1) = parseExpression ts
		let ts2 = match Rparen ts1 "factor"
		(e, ts2)
parseFactor ((Id str, l1, c1):(Lparen, l2, c2):ts) = parseCall ((Id str, l1, c1):(Lparen, l2, c2):ts)
parseFactor ((Id str, l, c):ts) = parseVar ((Id str, l, c):ts)
parseFactor ((Num n, _, _):ts) = (NumExp n, ts)
parseFactor ts = syntaxError ts "factor"

-- call
parseCall :: [Token] -> (Exp, [Token])
parseCall ((Id str, l, c):(Lparen, _, _):ts) = 
	do
		let (arg, ts1) = parseArgs ts
		let ts2 = match Rparen ts1 "call"
		(CallExp (Id str, l, c) NullDecl arg, ts2)
parseCall ts = syntaxError ts "call"

-- args 
parseArgs :: [Token] -> ([Exp], [Token])
parseArgs ((Rparen, l, c):ts) = ([], (Rparen, l, c):ts)
parseArgs ts = parseArgList ts

-- arg-list
parseArgList :: [Token] -> ([Exp], [Token])
parseArgList ts = 
	do
		let (e, (t, l, c):ts1) = parseExpression ts
		let (es, ts2) = if t == Comma 
							then parseArgList ts1 
							else ([], (t, l, c):ts1)
		([e]++es, ts2)

-------------------------
-- expression Functions --
-------------------------

syntaxError t ruleName = error ("Syntax error near " ++ show (head t) ++ " while attempting to recognise \"" ++ ruleName ++ "\"\n\n")

match t1 ((t2,r,c):ts) funName = if t1 == t2 then ts else syntaxError [(t2,r,c)] funName